<?php

use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;


abstract class BaseController {
	
	protected $oView;
	
	private $oRequest;
	
	private $oResponse;
	
	public function __construct(Ace_Core_Registry $oRegistry, Request $oRequest, Response $oResponse) 
	{	
		$this->oView = $oRegistry;
		$this->oRequest = $oRequest;
		$this->oResponse = $oResponse;
		
		$this->oResponse->headers->set('X-XSS Protection', '1; mode=block');
		
	}
	
	protected function getResponse()
	{
		return $this->oResponse;
	}
	
	protected function getRequest()
	{
		return $this->oRequest;
	}
	
	public function render() {
		
		//Main template
		$sFileName = VIEW_ROOT . DIRECTORY_SEPARATOR .  'Main.php';
		
		if (file_exists($sFileName) == false) {
			throw new RunTimeException('Invalid filename.');
		}
		
		$aViewData = $this->oView->getData(); 
		
		foreach($aViewData as $sVarName => $sValue) {
			$$sVarName = $sValue;
		}
		
		$sTemplate = $this->oView->sControllerName . '/' . $this->oView->sMethodName;
		
		//Start buffering
		ob_start();
		
		//Include file
		include($sFileName);
		
		//Get the contents of the buffer
		$sContent = ob_get_contents();
		
		$this->getResponse()->setContent($sContent);
		
		ob_end_clean();
	}
	
	public function renderTemplate($sTemplateName)
	{
		$sFileName = VIEW_ROOT . DIRECTORY_SEPARATOR . $sTemplateName . '.php';
	
		if (file_exists($sFileName) == false) {
			throw new RunTimeException('Invalid filename.');
		}
		
		include($sFileName);
	}
}
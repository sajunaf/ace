<?php

function dd()
{
	echo '<pre>';
	array_map(function ($x) {
		print_r($x);
	}, func_get_args());
		die;
}

define('PROJECT_URL', 'http://ace.local/');

define('PROJECT_ROOT', '/var/www/ace');

define('MODEL_ROOT', PROJECT_ROOT . DIRECTORY_SEPARATOR . 'models');

define('VIEW_ROOT', PROJECT_ROOT . DIRECTORY_SEPARATOR . 'views');

define('CONTROLLER_ROOT', PROJECT_ROOT . DIRECTORY_SEPARATOR . 'controllers');

define('VENDOR_ROOT', PROJECT_ROOT . DIRECTORY_SEPARATOR . 'vendor');
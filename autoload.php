<?php


function autoload($class_name) {
	
	$sClassName = str_replace(array('Ace', '_'), array('', DIRECTORY_SEPARATOR), $class_name);
	
	$sFileName = '';
	
	if(strpos($sClassName, 'Controller')) {
		
		$sFileName = CONTROLLER_ROOT . DIRECTORY_SEPARATOR .str_replace('Controller', '', $sClassName) . '.php';
		
		if (file_exists($sFileName) == false) {
			throw new Ace_Exception_InvalidRoute('Could not find the page you are looking for');
		}
		
	} else {
		
		$sFileName = MODEL_ROOT . DIRECTORY_SEPARATOR . $sClassName . '.php';
		
	}
	
	if (file_exists($sFileName) == false) {
		throw new RunTimeException('Invalid file.');
	}
	
	require_once($sFileName);
	
}

spl_autoload_register('autoload');

include VENDOR_ROOT . DIRECTORY_SEPARATOR . 'autoload.php';

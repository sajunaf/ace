<?php 

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);
ini_set('display_errors', 'on');
opcache_reset();

use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;
use Symfony\Component\HttpFoundation\RedirectResponse as RedirectResponse;

include __DIR__ . '/defines.php';
include __DIR__ . '/autoload.php';

$oRequest = Request::createFromGlobals();
$oResponse = new Response();


try {
	
	$oRoute = new Ace_Core_Route();
	$oRoute->dispatch($oRequest, $oResponse);
	
} catch (Ace_Exception_InvalidRoute $oInvalidRouteException) {

	//if an invalid url we create a new RedirectResponse
	$oResponse = new RedirectResponse(PROJECT_URL . 'FileNotFound');
	
} catch (Exception $oException) {
	
	$oResponse->setContent($oException->getMessage());
}

$oResponse->send();
exit();






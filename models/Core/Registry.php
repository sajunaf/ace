<?php

class Ace_Core_Registry {
	
	private $aData = array();
	
	public function __set($index, $value) {
		$this->aData[$index] = $value;
	}
	
	public function __get($index) {
		return $this->aData[$index];
	}
	
	public function getData() {
		return $this->aData;
	}
	
}
<?php

use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;

class Ace_Core_Route {

	public function dispatch(Request $oRequest, Response $oResponse) 
	{
		
		$aQueryParams = array_filter(explode('/', $oRequest->query->get('q')));
		
		$sControllerName = $sDefaultController = 'Index';
		$sMethodName = $sDefaultMethod = 'Index';
		
		if(count($aQueryParams) > 0) {
			$sControllerName = $aQueryParams[0];	
		}		
		
		if(count($aQueryParams) > 1) {
			$sMethodName = $aQueryParams[1];	
		}
		
		$oRegistry = new Ace_Core_Registry();
		
		//Set controller and method name in the request to be used later in the views
		$oRegistry->sControllerName = $sControllerName;
		$oRegistry->sMethodName = $sMethodName;
		
		$sControllerName .= 'Controller';
		
		if (is_callable(array($sControllerName, $sMethodName)) == false) {	
			throw new RunTimeException('Invalid controller or method name');
		}
		
		$oController = new $sControllerName($oRegistry, $oRequest, $oResponse);
		$oController->$sMethodName();	
	}
}